# Discord Bot

FR | [EN](README-en_US.md)

Ceci est un bot créé avec la librarie discord.js version 13. Le but de ce projet est de fournir un solution clé en main, pour permettre à tout le monde d'avoir un Bot discord avec une identitée customisable, facilement et rapidement

## Guide de mise en route

### Prérequis

Node.js 16.9.0 ou plus récent - [Link](https://nodejs.org/en/download/) (Voir la documentation de node pour les instructions d'installation pour votre OS)

npm (Préinstallé par défaut avec node. Si ce n'est pas le cas, voir cet [article](https://docs.npmjs.com/cli/v8/configuring-npm/install))

[git](https://git-scm.com/downloads)

### Obtention du code et des dépendences

Dans un terminal

```sh
git clone https://framagit.com/loulou310/bot-discord
```

Sortie de la commande : To be added

```sh
cd bot-discord
npm i
```

Sortie de la commande : TBA

### Enregistrer un bot 

1. Allez sur ce site :  [https://discord.com/developers/applications](https://discord.com/developers/applications)
2. Si vous n'êtes pas connectés à Discord, connectez-vous
3. Appuyez sur le bouton "New Application" en haut à droite de la page
4. Choissisez un nom (Ce ne sera pas le nom de votre bot)
5. Dans le côté gauche, cliquez sur "Bot"
6. Dans la section "Bulid-a-bot", appuyez sur le bouton "Add Bot" et confirmez la création
7. Vous pouvez maintenant choisir le nom de votre bot et sa photo de profil
8. En dessous du champ du nom d'utilisateur, vous trouvrez la section "TOKEN". Appuyez sur "Reset Token", confirmez l'action et renseignez le code d'authentification à 2 facteurs si nécessaire
9. Appuyez sur le bouton "Copy" et stockez le dans un endroit ou vous allez le retrouver car vous pouvez le voir ici seulement une fois. Si vous ne le copiez pas maintenant, vous devrez reproduire les étapes 8 et 9 à nouveau

**Avertissement** : Ne partagez votre token à personne, **même a moi.**

### Ajoutez le bot a votre serveur

1. Sur le portail développeur, accedez l'application liée au bot
2. Allez dans la section "OAuth 2", puis dans "URL Generator"
3. Dans "Scopes", cochez les cases en face de "bot" et de "application.commands"
4. Dans "BOT PERMISSIONS", cochez les cases en face de "Kick members", "Ban members", "Moderate members", "Send messages", "Send messages in threads", "Manage Messages", "Embed links", "Read message history", "Add reactions". Vous pouvez seulement cocher la permission "Administrator", mais cela n'est pas recommandé pour des raisons de sécurité.
5. Copiez le lien en bas de la page et copiez le dans votre barre d'adresse. Suivez les étapes pour ajouter le bot à votre serveur

### Création du fichier de configuration

1. Ouvrez votre éditeur de texte favori.
2. Copiez les lignez suivantes

```json
{
    "token": "",
    "clientId": "",
    "guildId": ""

}
```

3. Mettez votre token dans les guillemets en face de "token"
4. Dans le portail dévelopeur Discord allez dans votre application. Dans la section "General Informations", cherchez "Application ID", et copiez la suite de nombres dans les guillemets en face de "clientID"
5. Vérifiez que le mode développeur est activé dans Discord. Pour l'activer, allez dans les paramètres Discord, puis dans avancés et activez le mode développeur
6. Faites un clique droit sur l'icone du serveur ou vous avez ajouté le bot, et cliquez sur "Copier l'identifiant"  
7. Enrigistrez ce fichier avec le nom `config.json`. Dans certains éditeurs de texte, il peut être nécessaire de sélectionner "Tous les fichiers" comme type de fichier

### Déployer les commandes

Dans un teminal ouvert dans le dossier du bot, exécutez

```sh
npm run deploy-commands
```

Sortie de la commande :

```
Successfully registered application commands.
```

Cela redra disponible les commandes en écrivant `/`

### Running bot

Dans un teminal ouvert dans le dossier du bot, exécutez 

```sh
npm run bot
```

Sortie de la commande : 

```
Ready! Logged in as <your bot's profile>
```

## Contact

Pour obtenir de l'aide, rejoignez [ce serveur Discord]()