const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('eval')
		.setDescription('For testing stuff only')
        .addStringOption(option => option.setName("eval").setDescription("Code to eval")),
	async execute(interaction, message, client) {
        function clean(text) {
            if (typeof text === "string") 
              return text.replace(/`/g, "`" + String.fromCharCode(8203)).replace(/@/g, "@" + String.fromCharCode(8203));
            return text;
          }
        
          if (interaction.member.id !== "336885637934481409") return;
          const code = interaction.options.getString("eval");
          console.log(code)
          const evaled = eval(code);
          console.log(evaled)
          const cleanCode = await clean(evaled);
          console.log(cleanCode)
          interaction.reply({content : `${cleanCode}`});
          //interaction.followUp({ code: "js"})
	},
};