const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('reload')
		.setDescription('Reload the bot'),
	async execute(interaction) {
		await interaction.reply({ content: 'Reloading bot, please wait', ephemeral: true});
        await process.kill(process.pid)
	},
};