const { SlashCommandBuilder } = require("@discordjs/builders")
const { EmbedBuilder } = require("discord.js")

module.exports = {
    data: new SlashCommandBuilder()
        .setName("timeout")
        .setDescription("Timeout the mentionned user")
        .addUserOption(option => option.setName('target').setDescription('Select a user').setRequired(true))
        .addIntegerOption(option => option.setName("minutes").setDescription("The number of minutes to ban the user"))
        .addStringOption(option => option.setName("reason").setDescription("The reason of the timeout (for loging only)")),  
    execute(interaction) {
        const userToBan = interaction.options.getUser('target')
        const reason = (interaction.options.getString('reason') || "No reason specified")
        const time = (interaction.options.getInteger('minutes') || 10)
        if (!userToBan) return
        userToBan ? interaction.guild.members.cache.get(userToBan.id).timeout(time * 60 * 1000, reason) : interaction.channel.send("L'utilisateur n'existe pas");

        const embed = new EmbedBuilder()
        .setAuthor({name: `${userToBan.tag} (${userToBan.id})`})
        .setColor("#ffa500")
        .setDescription(`**Action**: timeout\n**Raison**: ${reason}\n**Durée**: ${time} minutes`)
        .setThumbnail(userToBan.avatarURL())
        .setTimestamp()
        .setFooter({text: `${interaction.member.user.tag}`, iconURL: interaction.member.user.displayAvatarURL()});

        interaction.reply({ embeds: [embed] })
    }
}