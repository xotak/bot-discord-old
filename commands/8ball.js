const { SlashCommandBuilder } = require('@discordjs/builders');
const { EmbedBuilder } = require('discord.js')

module.exports = {
	data: new SlashCommandBuilder()
		.setName('8ball')
		.setDescription('Does a prediction')
        .addStringOption(option => option.setName("prediction").setDescription("The prediction you want to ask the 8ball").setRequired(true)),
	async execute(interaction) {
    const replies = ["D'après moi oui", "C'est certain", "Oui", "Faut pas rêver", "C'est ton destin", "C'est non", "Peu probable"]
    const response = Math.floor(Math.random() * replies.length)

    const embed = new EmbedBuilder()
        .setAuthor({name: `${interaction.member.user.username}#${interaction.member.user.discriminator}`})
        .setColor('#000000')
        .setThumbnail("https://s1.qwant.com/thumbr/0x380/a/d/afc6eb8d50a8334b34f775817851636d4b32d70ab2b0731660566ae2b3474d/8-ball-png-8.jpg?u=https%3A%2F%2Fclipground.com%2Fimages%2F8-ball-png-8.jpg&q=0&b=1&p=0&a=1")
        .addFields({name:`${interaction.options.getString('prediction')}`, value: replies[response]})

        interaction.reply({ embeds: [embed] })
	}

    
};