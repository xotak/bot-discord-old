const { SlashCommandBuilder } = require("@discordjs/builders")
const { EmbedBuilder } = require("discord.js")

module.exports = {
    data: new SlashCommandBuilder()
        .setName("ban")
        .setDescription("Bans the mentionned user")
        .addUserOption(option => option.setName('target').setDescription('Select a user').setRequired(true))
        .addIntegerOption(option => option.setName("days").setDescription("The number of days to ban the user"))
        .addStringOption(option => option.setName("reason").setDescription("The reason of the kick (for loging only)")),  
    execute(interaction) {
        const userToBan = interaction.options.getUser('target')
        const reason = (interaction.options.getString('reason') || "No reason specified")
        const days = interaction.options.getInteger('days')
        if (!userToBan) return
        userToBan ? interaction.guild.members.cache.get(userToBan.id).ban({days : days, reason: reason}) : interaction.channel.send("L'utilisateur n'existe pas");

        const embed = new EmbedBuilder()
        .setAuthor({name: `${userToBan.tag} (${userToBan.id})`})
        .setColor("#ffa500")
        .setDescription(`**Action**: ban\n**Raison**: ${reason}\n**Durée**: ${days} jours`)
        .setThumbnail(userToBan.avatarURL())
        .setTimestamp()
        .setFooter({text: `${interaction.member.user.tag}`, iconURL: interaction.member.user.displayAvatarURL()});

        interaction.reply({ embeds: [embed] })
    }
}