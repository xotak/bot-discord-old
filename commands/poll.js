const { SlashCommandBuilder } = require('@discordjs/builders');
const { EmbedBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('poll')
		.setDescription('Create a poll with reactions')
        .addStringOption(option => option.setName("question").setDescription("The question of the poll").setRequired(true)),
	async execute(interaction) {
		const embed = new EmbedBuilder()
        .setAuthor({name: `${interaction.member.user.tag}`})
        .setColor('#000000')
        .setDescription(`${interaction.options.getString('question')}`)
        .addFields({name: "Utilisez les réaction pour donner votre avis sur la question suivante", value:
        `
        🟩 - Pour (Oui)
        🟦 - Neutre
        🟥 - Contre (Non)
        `}
        )
        .setTimestamp()

        interaction.reply({ content: 'Generating poll, please wait...', ephemeral: true})

        const poll = await interaction.channel.send({ embeds: [embed] })

        await poll.react("🟩")
        await poll.react("🟦")
        await poll.react("🟥")
	},
};