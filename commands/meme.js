const { SlashCommandBuilder } = require('@discordjs/builders');
const { EmbedBuilder } = require("discord.js")
const fetch = require("node-fetch")
module.exports = {
	data: new SlashCommandBuilder()
		.setName('meme')
		.setDescription('Replies with a random meme taken from r/memes'),
	async execute(interaction) {
        
        const meme = await fetch("https://www.reddit.com/user/loulou310/m/memes/top/.json?sort=top&ampt=day&amplimit=100")
        .then(res => res.json())
        .then(json => json.data.children)
        
        
    
        const img = meme[Math.floor(Math.random() * meme.length)].data
        const embed = new EmbedBuilder()
        .setDescription(img.title)
        .setImage(img.url)
        .setFooter({text: `Powered by ${img.subreddit_name_prefixed}`})
    
        await interaction.deferReply()
        await interaction.editReply({ embeds: [embed] })
	},
};