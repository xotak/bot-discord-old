const { SlashCommandBuilder } = require("@discordjs/builders")
const { EmbedBuilder } = require("discord.js")

module.exports = {
    data: new SlashCommandBuilder()
        .setName("kick")
        .setDescription("Kicks the mentionned user")
        .addUserOption(option => option.setName('target').setDescription('Select a user').setRequired(true))
        .addStringOption(option => option.setName("reason").setDescription("The reason of the kick (for loging only)")),  
    execute(interaction) {
        const userToBan = interaction.options.getUser('target')
        const reason = (interaction.options.getString('reason') || "No reason specified")
        if (!userToBan) return
        userToBan ? interaction.guild.members.cache.get(userToBan.id).kick(reason) : interaction.channel.send("L'utilisateur n'existe pas");

        const embed = new EmbedBuilder()
        .setAuthor({name: `${userToBan.tag} (${userToBan.id})`})
        .setColor("#ffa500")
        .setDescription(`**Action**: kick\n**Raison**: ${reason}`)
        .setThumbnail(userToBan.avatarURL())
        .setTimestamp()
        .setFooter({text: `${interaction.member.user.tag}`, iconURL: interaction.member.user.displayAvatarURL()});

        interaction.reply({ embeds: [embed] })
    }
}