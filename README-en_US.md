# Discord Bot

[FR](README.md) | EN

This is a Discord bot made with the library discord.js version 13. The goal of this project is to allow everyone to have a Discord bot with a customizable identity easily and quickly.

## Set-up guide

### Prerequisite

Node.js 16.9.0 or newer - [Link](https://nodejs.org/en/download/) (See node documentation for install instruction for your specific OS)

npm (Pre-installed by default with node. If not, see [this](https://docs.npmjs.com/cli/v8/configuring-npm/install))

[git](https://git-scm.com/downloads)

### Obtaining code and depedencies

In a terminal

```sh
git clone https://framagit.com/loulou310/bot-discord
```

Command output : To be added

```sh
cd bot-discord
npm i
```

Command output : TBA

### Registering a bot

1. Go to [https://discord.com/developers/applications](https://discord.com/developers/applications)
2. If not logged in your Discord account, log in
3. Click on the "New Application" button in the top right corner
4. Pick a name (This is not gonna be the name of your bot)
5. On the left side, click on "Bot"
6. In the "Bulid-a-bot" section, click on the "Add Bot" button, and confirm when prompted
7. You can now choose a profile picture and a username
8. Under the username field, you will find the "TOKEN" section. Click on the reset token button, confirm and input 2FA code if prompted
9. Click on the copy button, and store the token somewere safe, you can see it only once here. If you don't copy it now, you will need to repeat the step 8 and 9 again.

**Disclaimer** : Do **not** share your token with anybody, **even me**

### Adding your bot in the server

1. On the developer portal, go to your aplication
2. Navigate to the OAuth2 section and then go to "URL Generator"
3. In "Scopes" check the box in front of "bot" and "application.commands"
4. In "BOT PERMISSIONS", check the boxes in front of "Kick members", "Ban members", "Moderate members", "Send messages", "Send messages in threads", "Manage Messages", "Embed links", "Read message history", "Add reactions". You can also ckeck only "Administrator" permission, but it's not recomended for security reasons.
5. Copy the link at the bottom of the page, and follow the steps. 

### Creating config file

1. Open your favorite text editor. It can be anything.
2. Copy the next lines

```json
{
    "token": "",
    "clientId": "",
    "guildId": ""

}
```

3. Put you token in the the quotations marks in front of "token"
4. On the Discord developer portal, go in your application. In the section "General Informations", search for "Application ID", and paste that between the quotations marks in front of "clientID"
5. Make sure developer mode is activated in Discord. If not sure, go in User Settings then Advanced, and toggle Devoloper Mode
6. Right click on the icon of the server you just added the bot, click on "Copy ID", and finally paste between the quotation marks in front of "guildID
7. Save this file as `config.json`. On some text editors, it might be required to select "All files" in the "Save as type" field.

### Deploying commands

In the bot's code folder, run in your favorite terminal

```sh
npm run deploy_command
```

Command output :

```
Successfully registered application commands.
```

This will make the commands available when typing `/`

### Running bot

Run in your favorite terminal,

```sh
npm run bot
```

Command output : 

```
Ready! Logged in as <your bot's profile>
```

## Contact

For troubleshooting, please join [this Discord server]()