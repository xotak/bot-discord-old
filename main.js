// Require the necessary libraries and config file
const fs = require('fs')
const { Client, Collection, GatewayIntentBits } = require('discord.js');
const { token } = require('./config.json');
// Create a new client instance
const client = new Client({ intents: [GatewayIntentBits.Guilds ,GatewayIntentBits.GuildMessages, GatewayIntentBits.MessageContent, GatewayIntentBits.GuildMembers] });

require("./utils/functions")(client)

//read commands from command files
client.commands = new Collection();
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

const eventFiles = fs.readdirSync('./events').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	// Set a new item in the Collection
	// With the key as the command name and the value as the exported module
	client.commands.set(command.data.name, command);
}

for (const file of eventFiles) {
	const event = require(`./events/${file}`);
	if (event.once) {
		client.once(event.name, (...args) => event.execute(...args));
	} else {
		client.on(event.name, (...args) => event.execute(...args));
	}
}

client.mongoose = require("./utils/mongoose.js")
client.mongoose.init()

client.on('messageCreate', async message => {
    let dbUser = await client.getUser(message.author)

    if (message.channel.type === 'dm') return client.emit("directMessage", message);

    if (message.author.bot) return;

    if (!dbUser) await client.createUser({
        guildID: message.member.guild.id,
        guildName: message.member.guild.name,
        userID: message.member.id,
        username: message.member.user.tag,
    }) 
	else {
    const expCd = Math.floor(Math.random() * 19) + 1;
    const expToAdd = Math.floor(Math.random() * 25) + 10 

    if (expCd >= 8 && expCd <= 12 ) {
        await client.addExp(client, message.member, expToAdd);
    };

    const userLevel = Math.floor(0.1 * Math.sqrt(dbUser.experience))
    if (dbUser.level < userLevel) {
        message.reply(`Tu viens de passer niveau ${userLevel}`)
        client.updateUser(message.member, { level : userLevel})
    }
}
});

client.on('guildCreate', async guild => {

		const newGuild = {
		guildID: guild.id,
		guildName: guild.name
	}
	await client.createGuild(newGuild)
	}
)

// Login to Discord with your client's token
client.login(token);